import { Modal } from 'bootstrap';
import { TabGroupRule } from './classes/tabgroup-rule.class';
import { ModalRuleAddElement } from './components/modal.rule-add.component';
import { IGetModalConfig } from './interfaces/get-modal-config.interface';

/**
 * Returns the value of the form as an Object identified by `T`
 * @param {string | HTMLFormElement} target - Target HTML form to return data from. Can be either a CSS selector, or a
 * HTMLFormElement instance
 * @returns Returns the form data, or null if the form was not found
 */
export function getFormValue<T>(target: string | HTMLFormElement): (T | null) {
    const element = target instanceof HTMLFormElement ? target : document.querySelector<HTMLFormElement>(target);

    if (element) {
        const fd = new FormData(element);
        return Object.fromEntries(fd) as T;
    }

    return null;
}

/**
 * Get a modal from either a CSS selector, or an HTML Element
 * @param {string|HTMLElement} selector - CSS selector, or HTMLElement object
 * @param {Partial<IGetModalConfig>=} config - (Optional) Modal configuration
 * @returns {Modal} - Reference to the modal
 * @throws Error - Thrown if the provided selector does not yield a valid HTML Element
 */
export function getModal(selector: string | HTMLElement, config?: Partial<IGetModalConfig>): Modal {
    const el = selector instanceof HTMLElement ? selector : document.querySelector(selector);

    if (!el) {
        throw new Error(`Failed to locate modal element with selector ${selector}`);
    }

    const modalConfig: Modal.Options = {
        keyboard: true,
        backdrop: 'static',
        focus: true,
        ...config?.modalConfig,
    };
    const modal = Modal.getOrCreateInstance(el, modalConfig);

    if (config?.events) {
        for (const evt of config.events) {
            el.addEventListener(evt.event.toString(), evt.callback);
        }
    }

    return modal;
}

/**
 * Setup form validation on the specified form element
 *
 * This function takes either an element selector, or an HTMLFormElement and will set up validation notifications on the form.
 * As part of the validation, any buttons with `type="submit"` will also be disabled if the form is invalid to avoid users
 * submitting the form before completely filling out all required fields
 *
 * @param {string | HTMLFormElement} selector - CSS Selector or HTMLFormElement instance
 */
export function setupValidation(selector: string | HTMLFormElement) {
    const form = selector instanceof HTMLFormElement ? selector : document.querySelector<HTMLFormElement>(selector);
    const btnSubmit = form?.querySelector<HTMLButtonElement>('button[type="submit"]');

    if (form && btnSubmit) {
        btnSubmit.disabled = !form.checkValidity();
        if (!form.classList.contains('was-validated')) {
            form.classList.remove('needs-validation');
            form.classList.add('was-validated');
        }

        form.addEventListener('input', () => {
            btnSubmit.disabled = !form.checkValidity();
            if (!form.classList.contains('was-validated')) {
                form.classList.remove('needs-validation');
                form.classList.add('was-validated');
            }
        });
    }
}

export function openRuleEditor(mode: 'add' | 'edit', groupId: string, rule?: TabGroupRule) {
    const contentDiv = document.querySelector<HTMLElement>('body > *:first-child');
    if (contentDiv) {
        const ruleAddEl = new ModalRuleAddElement();
        ruleAddEl.groupId = groupId;

        if (mode === 'edit' && rule) {
            ruleAddEl.domain = rule.domain;
            ruleAddEl.rule = rule;
            ruleAddEl.mode = 'edit';
        }
        contentDiv.appendChild(ruleAddEl);

        const modalEl = ruleAddEl.querySelector<HTMLDivElement>('div.modal');
        const form = ruleAddEl.querySelector<HTMLFormElement>('form');
        if (modalEl && form) {
            setupValidation(form);
            const modal = getModal(modalEl);

            if (modal) {
                modal.show();

                modalEl.addEventListener('hidden.bs.modal', () => {
                    contentDiv.removeChild(ruleAddEl);
                });
            }
        }
    }
}
