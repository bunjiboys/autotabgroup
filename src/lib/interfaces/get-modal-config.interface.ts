import { Modal } from 'bootstrap';


interface IModalEvents {
    event: Modal.Events;
    callback: EventListenerOrEventListenerObject;
}

/**
 * Configuration object for getModal()
 * @property {IModalEvents[]} events - A list of event subscriptions for the modal
 * @property {Modal.Options} modalConfig - Modal configuration
 */
export interface IGetModalConfig {

    events: IModalEvents[];

    modalConfig: Modal.Options;
}
