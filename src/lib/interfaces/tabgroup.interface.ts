import { TabGroup } from '../classes/tabgroup.class';
import { TabGroupColor } from './colors';

export interface ITabGroupList {
    [id: string]: TabGroup;
}

export interface ITabGroupRulePath {
    path: string;
    recursive: boolean;
}

export interface ITabGroupRule {
    domain: string;
    paths: ITabGroupRulePath[];
    includeWildcard: boolean;
}

export interface ITabGroup {
    id: string;
    name: string;
    rules: ITabGroupRule[];
    color: TabGroupColor;
}

