import { Observable } from 'rxjs';
import { ITabGroup } from './tabgroup.interface';

export interface IConfiguration {
    tabGroups: { [id: string]: ITabGroup };
    overrideGroup: boolean;
    update$: Observable<boolean>;
}

export interface IRawConfiguration {
    tabGroups: ITabGroup[];
    overrideGroup: boolean;
}
