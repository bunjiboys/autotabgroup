import { TabGroupColor } from './colors';

export interface ITabGroupForm {
    name: string;
    color: TabGroupColor;
}

export interface ITabGroupRuleForm {
    [key: string]: string | boolean;

    domain: string;
    includeWildcard: boolean;
    path0: string;
    recursive0: boolean;
    path1: string;
    recursive1: boolean;
    path2: string;
    recursive2: boolean;
    path3: string;
    recursive3: boolean;
    path4: string;
    recursive4: boolean;
    path5: string;
    recursive5: boolean;
    path6: string;
    recursive6: boolean;
    path7: string;
    recursive7: boolean;
    path8: string;
    recursive8: boolean;
    path9: string;
    recursive9: boolean;
}
