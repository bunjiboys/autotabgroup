export type TabGroupColor =
    'grey' |
    'blue' |
    'red' |
    'yellow' |
    'green' |
    'pink' |
    'purple' |
    'cyan' |
    'orange';

export type NotifyColors =
    'primary' |
    'secondary' |
    'success' |
    'danger' |
    'warning' |
    'info' |
    'light' |
    'dark';
