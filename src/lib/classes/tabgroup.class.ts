import { TabGroupColor } from '../interfaces/colors';
import { ITabGroup } from '../interfaces/tabgroup.interface';
import { TabGroupRule } from './tabgroup-rule.class';

export class TabGroup implements ITabGroup {
    constructor(public id: string, public name: string, public color: TabGroupColor, public rules: TabGroupRule[]) {
    }

    // region Utilities
    matches(url: string): boolean {
        for (const rule of Object.values(this.rules)) {
            if (rule.matches(url)) {
                return true;
            }
        }

        return false;
    }

    serialize(): ITabGroup {
        return {
            id: this.id,
            name: this.name,
            color: this.color as TabGroupColor,
            rules: this.rules.map(itm => itm.serialize()),
        };
    }

    static deserialize(data: ITabGroup): TabGroup {
        return new TabGroup(
            data.id,
            data.name,
            data.color,
            data.rules.map(itm => TabGroupRule.deserialize(itm)),
        );
    }

    // endregion
}
