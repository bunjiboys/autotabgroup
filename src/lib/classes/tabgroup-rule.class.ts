import { ITabGroupRule } from '../interfaces/tabgroup.interface';
import { TabGroupRulePath } from './tabgroup-rule-path.class';

export class TabGroupRule implements ITabGroupRule {
    constructor(public domain: string, public paths: TabGroupRulePath[], public includeWildcard: boolean) {
    }

    // region Properties
    get patterns(): RegExp[] {
        const patterns: RegExp[] = [];

        const domains = [this.domain];
        if (this.includeWildcard) {
            // eslint-disable-next-line no-useless-escape
            domains.push(`(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])\.)?${this.domain}`);
        }
        for (const domain of domains) {
            if (this.paths.length) {
                for (const path of Object.values(this.paths).map(itm => itm.rgxPattern)) {
                    patterns.push(new RegExp(`https?://${domain}/${path}`));
                }
            } else {
                patterns.push(new RegExp(`https?://${domain}/.*`));
            }
        }

        return patterns;
    }

    // endregion

    toString(): string {
        return this.includeWildcard ? `*.${this.domain}` : this.domain;
    }

    equals(other: TabGroupRule): boolean {
        if (this.domain !== other.domain) {
            return false;
        }

        if (this.includeWildcard !== other.includeWildcard) {
            return false;
        }

        if (this.paths.length !== other.paths.length) {
            return false;
        }

        for (const configPath of this.paths) {
            if (other.paths.filter(itm => itm.rgxPattern === configPath.rgxPattern).length === 0) {
                return false;
            }
        }

        return true;
    }

    matches(url: string) {
        for (const pattern of this.patterns) {
            if (url.toString().match(pattern)) {
                return true;
            }
        }

        return false;
    }

    serialize(): ITabGroupRule {
        return {
            domain: this.domain,
            paths: this.paths.map(itm => itm.serialize()),
            includeWildcard: this.includeWildcard,
        };
    }

    static deserialize(data: ITabGroupRule): TabGroupRule {
        return new TabGroupRule(
            data.domain,
            data.paths?.map(itm => TabGroupRulePath.deserialize(itm)) || [],
            data.includeWildcard,
        );
    }
}
