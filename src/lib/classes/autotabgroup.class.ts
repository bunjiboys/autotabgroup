import { catchError, NEVER } from 'rxjs';
import { AlertsElement } from '../components/alert.component';
import { ModalConfirmElement } from '../components/modal.confirm.component';
import { ModalGroupAddElement } from '../components/modal.group-add.component';
import { ModalImexElement } from '../components/modal.imex.component';
import { TabGroupListElement } from '../components/tabgroup-list.component';
import {
    AlertEvent,
    ConfirmEvent,
    EventMap,
    GroupAddEvent,
    GroupEvent,
    IntentGroupAdd,
    IntentPathDelete,
    IntentRuleAdd,
    IntentRuleDelete,
    IntentRuleEdit,
    RuleUpdateEvent,
} from '../events';
import { IConfigDeletePath, IConfigDeleteRule, IConfigDeleteTabgroup } from '../events/confirm.events';
import { getModal, openRuleEditor, setupValidation } from '../utils';
import { Configuration, getConfig } from './configuration.class';

export class AutoTabGroup {
    private config: Configuration;
    private readonly alerts: AlertsElement;

    constructor() {
        this.config = getConfig();
        this.alerts = new AlertsElement();
    }

    // region Initialization
    /**
     * Executes the initialization methods, setting up a config reloader on changes to re-render the page
     */
    public init() {
        this.registerEvents();

        this.config.update$.pipe(
            catchError(() => NEVER),
        ).subscribe(this.render.bind(this));
    }

    /**
     * Renders the contents of the page
     * @private
     */
    private render() {
        const groupContainer = document.querySelector<HTMLDivElement>('#tabgroup-list');
        groupContainer?.replaceChildren();

        const groupList = new TabGroupListElement();
        if (groupContainer) {
            groupList.tabGroups = this.config.tabGroupList;
            groupContainer.appendChild(groupList);
        }
    }

    /**
     * Register all event handlers
     * @private
     */
    private registerEvents() {
        const contentDiv = document.querySelector<HTMLDivElement>('body > *:first-child');

        if (contentDiv) {
            contentDiv.appendChild(this.alerts);

            // Intents
            contentDiv.addEventListener(EventMap.IntentRuleAdd, this.intentRuleAdd.bind(this));
            contentDiv.addEventListener(EventMap.IntentRuleEdit, this.intentRuleEdit.bind(this));
            contentDiv.addEventListener(EventMap.IntentRuleDelete, this.intentRuleDelete.bind(this));
            contentDiv.addEventListener(EventMap.IntentPathDelete, this.intentPathDelete.bind(this));
            contentDiv.addEventListener(EventMap.IntentGroupAdd, this.intentGroupAdd.bind(this));
            contentDiv.addEventListener(EventMap.IntentGroupDelete, this.intentGroupDelete.bind(this));
            contentDiv.addEventListener(EventMap.IntentImportExport, this.intentImex.bind(this));
            contentDiv.addEventListener(EventMap.IntentToggleOverride, this.intentToggleOverride.bind(this));

            // Actions
            contentDiv.addEventListener(EventMap.RuleUpdate, this.eventRuleUpdate.bind(this));
            contentDiv.addEventListener(EventMap.GroupAdd, this.eventGroupAdd.bind(this));

            // Modals
            contentDiv.addEventListener(EventMap.ModalConfirm, this.eventModalConfirm.bind(this));

            // Notifications
            contentDiv.addEventListener(EventMap.Alert, this.eventAlert.bind(this));
        }
    }

    // endregion

    // region Intent Handlers
    /* eslint-disable class-methods-use-this */
    /**
     * Handle a user request to delete a group after a confirmation modal has been displayed
     * @param {GroupEvent} evt - Event containing relevant group information
     * @private
     */
    private intentGroupDelete(evt: GroupEvent) {
        evt.stopPropagation();

        const contentDiv = document.querySelector<HTMLElement>('body > *:first-child');
        if (contentDiv) {
            const confirmEl = new ModalConfirmElement();
            (evt.target as HTMLButtonElement).disabled = true;
            confirmEl.context = {action: 'deleteTabgroup', groupId: evt.detail.groupId} as IConfigDeleteTabgroup;
            confirmEl.modalId = `confirm-${Math.round(Math.random() * 10000 + Date.now())}`;
            confirmEl.modalTitle = 'Override Groups';
            confirmEl.modalBody = `Are you sure you want to delete the tabgroup and all rules? This cannot be undone`;
            contentDiv.appendChild(confirmEl);

            const modalEl = confirmEl.querySelector<HTMLDivElement>('.modal');
            if (modalEl) {
                const modal = getModal(modalEl);
                if (modal) {
                    modal.show();

                    confirmEl.addEventListener('hidden.bs.modal', () => {
                        contentDiv.removeChild(confirmEl);
                        (evt.target as HTMLButtonElement).disabled = false;
                    });
                }
            }
        }
    }

    /**
     * Handle a user requesting to add a new tabgroup, opening the create modal
     * @param {IntentGroupAdd} evt - Event containing relevant group information
     * @private
     */
    private intentGroupAdd(evt: IntentGroupAdd) {
        evt.stopPropagation();

        const contentDiv = document.querySelector<HTMLElement>('body > *:first-child');

        if (contentDiv) {
            const parentEl = new ModalGroupAddElement();
            contentDiv.appendChild(parentEl);

            const modalEl = parentEl.querySelector<HTMLDivElement>('.modal');
            const frmGroupAdd = modalEl?.querySelector<HTMLFormElement>('form');

            if (modalEl && frmGroupAdd) {
                setupValidation(frmGroupAdd);

                // Make sure we remove the modal element from the DOM when the modal is fully hidden
                modalEl.addEventListener('hidden.bs.modal', () => {
                    contentDiv.removeChild(parentEl);
                });

                const modal = getModal(modalEl);
                if (modal) {
                    modal.show();
                }
            }
        }
    }

    /**
     * Handle a user requesting to delete a path from a rule, after a confirmation modal
     * @param {IntentPathDelete} evt - Event containing relevant group information
     * @private
     */
    private intentPathDelete(evt: IntentPathDelete) {
        evt.stopPropagation();

        const contentDiv = document.querySelector<HTMLElement>('body > *:first-child');
        if (contentDiv) {
            const confirmEl = new ModalConfirmElement();
            confirmEl.context = {action: 'deletePath', groupId: evt.groupId, rule: evt.rule, path: evt.path} as IConfigDeletePath;
            confirmEl.modalId = `confirm-${Math.round(Math.random() * 10000 + Date.now())}`;
            confirmEl.modalTitle = 'Delete rule path?';
            confirmEl.modalBody = `Are you sure you want to delete the path ${evt.detail.path.pattern} from ${evt.detail.rule.toString()}`;
            contentDiv.appendChild(confirmEl);

            const modalEl = confirmEl.querySelector<HTMLDivElement>('.modal');
            if (modalEl) {
                const modal = getModal(modalEl);
                if (modal) {
                    modal.show();

                    confirmEl.addEventListener('hidden.bs.modal', () => {
                        contentDiv.removeChild(confirmEl);
                    });
                }
            }
        }
    }

    /**
     * Handle a user requesting to delete a rule from a tabgroup, after a confirmation modal
     * @param {IntentRuleDelete} evt - Event containing relevant group information
     * @private
     */
    private intentRuleDelete(evt: IntentRuleDelete) {
        evt.stopPropagation();

        console.log('meep');
        const contentDiv = document.querySelector<HTMLElement>('body > *:first-child');
        if (contentDiv) {
            const confirmEl = new ModalConfirmElement();
            confirmEl.context = {action: 'deleteRule', groupId: evt.detail.groupId, rule: evt.detail.rule} as IConfigDeleteRule;
            confirmEl.modalId = `confirm-${Math.round(Math.random() * 10000 + Date.now())}`;
            confirmEl.modalTitle = 'Delete rule?';
            confirmEl.modalBody = `Are you sure you want to delete the rule ${evt.detail.rule.toString()}? This cannot be undone!`;
            contentDiv.appendChild(confirmEl);

            const modalEl = confirmEl.querySelector<HTMLDivElement>('.modal');
            if (modalEl) {
                const modal = getModal(modalEl);
                if (modal) {
                    modal.show();

                    confirmEl.addEventListener('hidden.bs.modal', () => {
                        contentDiv.removeChild(confirmEl);
                    });
                }
            }
        }
    }

    /**
     * Handle a user requesting to add a new rule to a tabgroup
     * @param {IntentRuleAdd} evt - Event containing relevant group information
     * @private
     */
    private intentRuleAdd(evt: IntentRuleAdd) {
        evt.preventDefault();
        evt.stopPropagation();

        openRuleEditor('add', evt.detail.groupId);
    }

    /**
     * Handle a user requesting to edit a rule, opening the rule editor window
     * @param {IntentRuleEdit} evt - Event containing relevant group information
     * @private
     */
    private intentRuleEdit(evt: IntentRuleEdit) {
        evt.preventDefault();
        evt.stopPropagation();

        openRuleEditor('edit', evt.detail.groupId, evt.detail.rule);
    }

    /**
     * Handle a user clicking the Import/Export configuration button, opening the imex modal
     * @param {Event} evt - Event containing relevant group information
     * @private
     */
    private intentImex(evt: Event) {
        evt.preventDefault();
        evt.stopPropagation();

        const contentDiv = document.querySelector<HTMLElement>('body > *:first-child');
        if (contentDiv) {
            const imex = new ModalImexElement();
            contentDiv.appendChild(imex);

            const modalEl = imex.querySelector<HTMLDivElement>('.modal');
            if (modalEl) {
                const modal = getModal(modalEl);
                if (modal) {
                    modal.show();

                    imex.addEventListener('hidden.bs.modal', (evt) => {
                        evt.preventDefault();
                        evt.stopPropagation();

                        contentDiv.removeChild(imex);
                    });
                }
            }
        }
    }

    /**
     * Handle a user requesting to view/edit the Override Group setting, opening the confirmation modal
     * @param {Event} evt
     * @private
     */
    private intentToggleOverride(evt: Event) {
        evt.preventDefault();
        evt.stopPropagation();

        const contentDiv = document.querySelector<HTMLElement>('body > *:first-child');
        if (contentDiv) {
            const confirmEl = new ModalConfirmElement();
            confirmEl.modalId = `confirm-${Math.round(Math.random() * 10000 + Date.now())}`;
            confirmEl.modalTitle = 'Override Groups';
            confirmEl.context = {
                action: 'setOverride',
            };
            const modalBody = document.createElement('div');

            modalBody.innerHTML = `
            <p>
                If enabled, Override Groups will re-assign the tab group and move the tab to a matching rule even if the tab is already assigned to another group. When disabled
                a tab group will only be updated if the tab is ungrouped.
            </p>
            <p>
                Current status: <b class="${this.config.overrideGroup ? 'text-success' : 'text-danger'}">${this.config.overrideGroup ? 'Enabled' : 'Disabled'}</b>
            </p>
            `;

            confirmEl.modalBody = modalBody;
            confirmEl.noButton = 'Close';
            confirmEl.yesButton = this.config.overrideGroup ? 'Disable' : 'Enable';
            contentDiv.appendChild(confirmEl);

            const modalEl = confirmEl.querySelector<HTMLDivElement>('.modal');
            if (modalEl) {
                const modal = getModal(modalEl);
                if (modal) {
                    modal.show();

                    confirmEl.addEventListener('hidden.bs.modal', () => {
                        contentDiv.removeChild(confirmEl);
                    });
                }
            }
        }
    }

    /* eslint-enable class-methods-use-this */
    // endregion

    // region Event handler functions
    /**
     * Handle a component requesting to display an alert to the user
     * @param {AlertEvent} evt - Alert event, containing the message and color of the alert to display
     * @private
     */
    private eventAlert(evt: AlertEvent) {
        this.alerts.notify(evt.message, evt.level);
    }

    /**
     * Handle a user submitting the Group Add form
     * @param {GroupAddEvent} evt - Event containing the group information
     * @private
     */
    private eventGroupAdd(evt: GroupAddEvent) {
        this.config.addTabGroup(evt.detail.name, evt.detail.color);
    }

    /**
     * Handle a user adding or editing a rule
     * @param {RuleUpdateEvent} evt - Event containing the rule information
     * @private
     */
    private eventRuleUpdate(evt: RuleUpdateEvent) {
        this.config.addOrEditGroupRule(evt.groupId, evt.rule, 'edit');
    }

    /**
     * Handle confirmation modal respones
     * @param {ConfirmEvent} evt - Event containing the action to perform, as well as any relevant context data
     * @private
     */
    private eventModalConfirm(evt: ConfirmEvent) {
        const contentDiv = document.querySelector<HTMLElement>('body > *:first-child');
        console.log(evt);

        if (contentDiv && evt.detail.result && evt.detail.context?.action) {
            let alert: AlertEvent | null = null;
            switch (evt.detail.context.action) {
                case 'setOverride': {
                    this.config.setOverrideGroup(!this.config.overrideGroup);
                    alert = new AlertEvent('Updated Override Group setting', 'success');
                    break;
                }

                case 'deleteTabgroup': {
                    const data = evt.context as IConfigDeleteTabgroup;
                    this.config.removeTabGroup(data.groupId);
                    alert = new AlertEvent('Tabgroup deleted', 'success');
                    break;
                }

                case 'deleteRule': {
                    const data = evt.context as IConfigDeleteRule;
                    this.config.removeRuleFromGroup(data.groupId, data.rule);
                    alert = new AlertEvent('Rule deleted', 'success');
                    break;
                }

                case 'deletePath': {
                    const data = evt.context as IConfigDeletePath;
                    this.config.removePathFromRule(data.groupId, data.rule, data.path);
                    alert = new AlertEvent('Path deleted', 'success');
                    break;
                }

                default: {
                    this.alerts.notify('Invalid request', 'info');
                    return;
                }
            }
            if (alert) {
                this.eventAlert(alert);
            }
        }
    }

    // endregion
}
