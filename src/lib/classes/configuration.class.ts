import { Observable, Subject } from 'rxjs';
import { v5 as uuidv5 } from 'uuid';
import { NotifyColors, TabGroupColor } from '../interfaces/colors';
import { IConfiguration, IRawConfiguration } from '../interfaces/configuration.interface';
import { TabGroupRulePath } from './tabgroup-rule-path.class';
import { TabGroupRule } from './tabgroup-rule.class';
import { TabGroup } from './tabgroup.class';
import StorageChange = chrome.storage.StorageChange;

const STORAGE_NS = '08a95eb8-e8b4-4175-85b2-8f31bb8e952a';

export class ConfigurationError extends Error {
    public color: NotifyColors;

    constructor(message: string, color?: NotifyColors) {
        super(message);
        this.color = color ? color : 'danger';
    }
}

/**
 * Configuration object used to manage the synchronized Chrome storage
 * @class
 * @property {{[p: string]: TabGroup}} tabGroups - TabGroups key/value object, keyed by TabGroup.id
 * @property {boolean} overrideGroup - When set to true, update the tab group on navigation, even if the tab is already in a tab group
 * @property {TabGroup[]} tabGroupList - TabGroups as a list
 * @property {Subject<boolean>} update$ - Observable that will emit a `true` value whenever the configuration changes
 */
export class Configuration implements IConfiguration {
    public tabGroups: { [id: string]: TabGroup } = {};
    public overrideGroup = false;
    private _update$ = new Subject<boolean>();

    constructor() {
        this.load();
    }

    // region Properties
    get tabGroupList(): TabGroup[] {
        return Object.values<TabGroup>(this.tabGroups);
    }

    get update$(): Observable<boolean> {
        return this._update$.asObservable();
    }

    setOverrideGroup(value: boolean) {
        this.overrideGroup = value;
        this.save();
    }

    // endregion

    /**
     * trackChanges sets up an automated reload whenever the underlying storage changes. Primarily used
     * to signal configuration changes to the serviceworker
     */
    trackChanges() {
        chrome.storage.sync.onChanged.addListener((changes: StorageChange) => {
            if ('autoTabGroup' in changes) {
                console.debug('Configuration change detected, reloading data');
                this.load();
            }
        });
    }

    // region TabGroups
    /**
     * Add a new tab group to the configuration
     * @param {string} name - Name of the tab group to create
     * @param {NotifyColors} color - Color of the tab group
     * @throws {ConfigurationError} - Raises a ConfigurationError on failures
     */
    addTabGroup(name: string, color: TabGroupColor) {
        if (name && color) {
            const grpId = generateTabgroupId(name);
            if (grpId in this.tabGroups) {
                throw new ConfigurationError(`Group ${name} already exists`, 'danger');
            }
            this.tabGroups[grpId] = new TabGroup(grpId, name, color, []);
            this.save().subscribe(err => {
                if (err) {
                    throw new ConfigurationError(``, 'danger');
                }
            });

            return;
        }

        throw new ConfigurationError('Name and/or color not provided');
    }

    /**
     * Remove an existing tab group fron the configuration
     *
     * Has no effect if the group id to delete does not exist in the configuration
     *
     * @param {string} groupId - Unique Group ID to remove from the configuration
     */
    removeTabGroup(groupId: string): Observable<string | null> {
        delete this.tabGroups[groupId];
        return this.save();
    }

    // endregion

    // region Rules
    /**
     * Update an existing tabgroup rule
     * @param {string} groupId - ID of the group owning the rule
     * @param {TabGroupRule} rule - Rule object
     * @param {('add'|'edit')} mode - Update mode, valid values: "add", "edit"
     */
    addOrEditGroupRule(groupId: string, rule: TabGroupRule, mode: 'add' | 'edit') {
        if (groupId in this.tabGroups) {
            const group = this.tabGroups[groupId];
            if (mode === 'edit') {
                const newRules = group.rules.filter(itm => itm.domain !== rule.domain);
                newRules.push(rule);
                group.rules = newRules;
            }
        }

        if (groupId in this.tabGroups) {
            const group = this.tabGroups[groupId];

            if (group.rules.map(itm => itm.equals(rule)).includes(true)) {
                if (mode === 'add') {
                    throw new ConfigurationError('Rule already exists');
                } else {
                    const newRules = group.rules.filter(itm => !itm.equals(rule));
                    newRules.push(rule);

                    this.tabGroups[groupId].rules = newRules;
                    this.save();
                }
            } else {
                if (mode === 'add') {
                    this.tabGroups[groupId].rules.push(rule);
                    this.save();
                } else {
                    throw new ConfigurationError('Rule updated failed');
                }
            }
        }
    }

    /**
     * Removes a rule from a tab group
     * @param {string} groupId - ID of the group owning the rule
     * @param {TabGroupRule} rule - Rule to delete
     */
    removeRuleFromGroup(groupId: string, rule: TabGroupRule) {
        if (groupId in this.tabGroups) {
            const group = this.tabGroups[groupId];

            group.rules = group.rules.filter(itm => !itm.equals(rule));
            this.save();
        }
    }

    /**
     * Removes a path from a rule
     * @param {string} groupId - ID of the group owning the rule
     * @param {TabGroupRule} rule - Rule to update
     * @param {TabGroupRulePath} path - Path to remove
     */
    removePathFromRule(groupId: string, rule: TabGroupRule, path: TabGroupRulePath) {
        if (groupId in this.tabGroups) {
            for (const rule of this.tabGroups[groupId].rules) {
                rule.paths = rule.paths.filter(itm => !itm.equals(path));
            }

            this.save();
        }
    }

    // endregion

    // region Configuration Management
    /**
     * Imports a configuration from a string
     * @param {string} configData - A JSON serialized configuration object
     * @throws {ConfigurationError} - Raises a ConfigurationError on failure
     */
    importConfiguration(configData: string) {
        try {
            const parsedData: IRawConfiguration = JSON.parse(configData);
            const newConfig = Configuration.deserialize(parsedData);

            this.tabGroups = newConfig.tabGroups;
            this.overrideGroup = newConfig.overrideGroup;
            this.save();

        } catch (ex) {
            if (ex instanceof SyntaxError) {
                throw new ConfigurationError('Failed loading invalid formatted configuration', 'danger');
            }

            throw ex;
        }
    }

    /**
     * Save the current configuration to the Sync storage area
     * @returns {Observable<string | null>} - Observable emitting a string if any errors occured, else null
     * @private
     */
    private save(): Observable<string | null> {
        const sub = new Subject<string | null>();
        const serialized = this.serialize();

        chrome.storage.sync.set({autoTabGroup: JSON.stringify(serialized)}, () => {
            sub.next(chrome.runtime.lastError && chrome.runtime.lastError.message ? chrome.runtime.lastError.message : null);
            sub.complete();
        });

        return sub.asObservable();
    }

    /**
     * Deserializes a configuration state from JSON, and returns a new Configuration instance
     * @param {IRawConfiguration} data - Data to load as configuration
     * @returns {Configuration}
     * @static
     */
    static deserialize(data: IRawConfiguration): Configuration {
        const config = new Configuration();

        for (const group of data.tabGroups) {
            config.tabGroups[group.id] = new TabGroup(
                group.id,
                group.name,
                group.color,
                group.rules.map(itm => TabGroupRule.deserialize(itm)),
            );
        }
        config.overrideGroup = data.overrideGroup;

        return config;
    }

    /**
     * Serializes the current configuration to a basic object for storage / exporting
     * @returns {IRawConfiguration} - Object
     */
    serialize(): IRawConfiguration {
        return {
            tabGroups: this.tabGroupList,
            overrideGroup: this.overrideGroup,
        };
    }

    /**
     * Loads the configuration from the chrome storage area, emitting a value to the update$ observable
     * @private
     */
    private load() {
        chrome.storage.sync.get('autoTabGroup', async (result) => {
            if (result && 'autoTabGroup' in result) {
                try {
                    const rawConfig: IRawConfiguration = JSON.parse(result.autoTabGroup);

                    this.tabGroups = {};
                    this.overrideGroup = false;

                    for (const group of rawConfig.tabGroups) {
                        this.tabGroups[group.id] = new TabGroup(
                            group.id,
                            group.name,
                            group.color,
                            group.rules.map(itm => TabGroupRule.deserialize(itm)),
                        );
                    }
                    this.overrideGroup = rawConfig.overrideGroup;

                    this._update$.next(true);
                } catch (e) {
                    console.error(`Got an error while trying to parse configuration: ${e}`);
                }
            } else {
                console.debug('No stored configuration found, initializing a new config');
            }
        });
    }

    // endregion
}

let _singleton: Configuration | null = null;

/**
 * Helper function to get a working Configuration instance. The optional callback will be executed
 * every time the configuration is modified
 * @param {(config: Configuration) => void} callback - Optional callback method to execute
 * @returns {Configuration}
 */
export function getConfig(callback?: (config: Configuration) => void) {
    if (!_singleton) {
        _singleton = new Configuration();
        _singleton.trackChanges();
    }

    if (callback) {
        _singleton.update$.subscribe(() => {
            callback(_singleton as Configuration);
        });
    }

    return _singleton;
}

/**
 * Generates a unique ID for a tabgroup
 *
 * Uses UUIDv5 with a constant namespace, which is a pure function (same output for the same input)
 * @param {string} name - Name of the tab group
 * @returns {string} - New ID based off the tab group name
 */
export function generateTabgroupId(name: string): string {
    return uuidv5(name.toLowerCase(), STORAGE_NS);
}
