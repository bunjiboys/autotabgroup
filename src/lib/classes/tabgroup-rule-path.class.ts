import { ITabGroupRulePath } from '../interfaces/tabgroup.interface';

export class TabGroupRulePath implements ITabGroupRulePath {
    constructor(public path: string, public recursive: boolean) {
    }

    get pattern(): string {
        if (this.recursive) {
            return `${this.path}*`;
        }

        return this.path;
    }

    get rgxPattern(): string {
        let pattern = this.pattern;
        if (pattern.startsWith('/')) {
            pattern = pattern.substring(1);
        }
        return pattern.replace('*', '.*').replace('/', '\\/');
    }

    equals(other: TabGroupRulePath) {
        return this.rgxPattern === other.rgxPattern;
    }

    serialize(): ITabGroupRulePath {
        return {
            path: this.path,
            recursive: false,
        };
    }

    static deserialize(data: ITabGroupRulePath): TabGroupRulePath {
        return new TabGroupRulePath(data.path, data.recursive);
    }
}
