import { AlertEvent } from './alerts.events';
import { ConfirmEvent } from './confirm.events';
import { GroupAddEvent, GroupEvent } from './groups.events';
import { IntentGroupAdd, IntentGroupDelete, IntentImportExport, IntentPathDelete, IntentRuleAdd, IntentRuleDelete, IntentRuleEdit, IntentToggleOverride } from './intents.events';
import { PathDeleteInternalEvent } from './paths.events';
import { RuleUpdateEvent } from './rules.events';

declare global {
    interface HTMLElementEventMap {
        // Intent Events
        'atg.intent.group.add': IntentGroupAdd;
        'atg.intent.group.delete': GroupEvent;
        'atg.intent.rule.add': IntentRuleAdd;
        'atg.intent.rule.delete': IntentRuleDelete;
        'atg.intent.rule.edit': IntentRuleEdit;
        'atg.intent.path.delete': IntentPathDelete;
        'atg.intent.imex': IntentImportExport;
        'atg.intent.toggleOverride': IntentToggleOverride;

        // Modal Events
        'atg.modal.confirm': ConfirmEvent,

        // Action Events
        'atg.rule.update': RuleUpdateEvent;
        'atg.group.add': GroupAddEvent;
        'atg.alert': AlertEvent;
        'atg.imex.import': Event;
        'atg.imex.export': Event;

        // Internal Events
        'atg.internal.path.delete': PathDeleteInternalEvent;
    }
}

export enum EventMap {
    // User clicked the "Add Rule" button
    IntentRuleAdd = 'atg.intent.rule.add',
    // User clicked the "New group" button
    IntentGroupAdd = 'atg.intent.group.add',
    // User clicked the "Delete group" button
    IntentGroupDelete = 'atg.intent.group.delete',
    // User clicked the "Delete Rule" icon
    IntentRuleDelete = 'atg.intent.rule.delete',
    // User clicked the "Edit Rule" icon
    IntentRuleEdit = 'atg.intent.rule.edit',
    // User clicked the "Path Delete" icon
    IntentPathDelete = 'atg.intent.path.delete',
    // User clicked the "Import / Export" button
    IntentImportExport = 'atg.intent.importExport',
    // User clicked the "Toggle Override" button
    IntentToggleOverride = 'atg.intent.toggleOverride',

    // Modal events
    ModalConfirm = 'atg.modal.confirm',

    // User submitted form update event
    RuleUpdate = 'atg.rule.update',
    // User submitted tab group create form
    GroupAdd = 'atg.group.add',
    // Group Action modal responmse
    GroupAction = 'atg.config.set.overrideGroup',

    // Other internally used events

    // Used when user clicks delete path to bubble up to parent for handling
    PathDeleteInternal1 = 'atg.internal.path.delete',

    // Used when a component needs to alert the user
    Alert = 'atg.alert',
}


export {
    GroupAddEvent,
    RuleUpdateEvent,
    AlertEvent,
    IntentGroupAdd,
    GroupEvent,
    ConfirmEvent,
    IntentRuleAdd,
    IntentRuleDelete,
    IntentRuleEdit,
    IntentPathDelete,
    IntentImportExport,
    IntentGroupDelete,
};

