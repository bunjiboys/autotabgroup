import { TabGroupRulePath } from '../classes/tabgroup-rule-path.class';
import { TabGroupRule } from '../classes/tabgroup-rule.class';
import { EventMap } from './index';

export type ConfigAction = 'setOverride' | 'deleteTabgroup' | 'deleteRule' | 'deletePath';

export interface IModalConfirmContext {
    action: ConfigAction;
}

export interface IConfigSetOverrideEvent extends IModalConfirmContext {
    overrideGroup: boolean;
}

export interface IConfigDeleteTabgroup extends IModalConfirmContext {
    groupId: string;
}

export interface IConfigDeleteRule extends IModalConfirmContext {
    groupId: string;
    rule: TabGroupRule;
}

export interface IConfigDeletePath extends IModalConfirmContext {
    groupId: string;
    rule: TabGroupRule;
    path: TabGroupRulePath;
}

export interface IConfirmEvent {
    result: boolean;
    modalId: string;
    context: IModalConfirmContext | null | undefined;
}

export class ConfirmEvent extends CustomEvent<IConfirmEvent> {
    constructor(public result: boolean, public modalId: string, public context: IModalConfirmContext | null | undefined) {
        super(EventMap.ModalConfirm, {
            bubbles: true,
            detail: {
                result,
                modalId,
                context,
            },
        });
    }
}
