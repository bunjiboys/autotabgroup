import { TabGroupColor } from '../interfaces/colors';
import { EventMap } from './index';

export interface GroupEventDetail {
    groupId: string;
}

export interface GroupAddEventDetail {
    name: string;
    color: TabGroupColor;
}

export class GroupEvent extends CustomEvent<GroupEventDetail> {
}

export class GroupAddEvent extends CustomEvent<GroupAddEventDetail> {
    constructor(public readonly name: string, public readonly color: TabGroupColor) {
        super(EventMap.GroupAdd, {bubbles: true, detail: {name, color}});
    }
}
