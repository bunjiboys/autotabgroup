import { TabGroupRulePath } from '../classes/tabgroup-rule-path.class';
import { TabGroupRule } from '../classes/tabgroup-rule.class';
import { GroupEventDetail } from './groups.events';
import { EventMap } from './index';
import { RuleEventDetail } from './rules.events';

export class IntentRuleAdd extends CustomEvent<GroupEventDetail> {
    constructor(groupId: string) {
        super(EventMap.IntentRuleAdd, {bubbles: true, detail: {groupId}});
    }
}

export class IntentGroupAdd extends Event {
    constructor() {
        super(EventMap.IntentGroupAdd, {bubbles: true});
    }
}

export class IntentGroupDelete extends CustomEvent<GroupEventDetail> {
    constructor(groupId: string) {
        super(EventMap.IntentGroupDelete, {bubbles: true, detail: {groupId}});
    }
}

export interface IntentPathDeleteDetail {
    groupId: string;
    rule: TabGroupRule;
    path: TabGroupRulePath;
}

export class IntentPathDelete extends CustomEvent<IntentPathDeleteDetail> {
    constructor(public groupId: string, public rule: TabGroupRule, public path: TabGroupRulePath) {
        super(EventMap.IntentPathDelete, {
            bubbles: true,
            detail: {
                groupId,
                rule,
                path,
            },
        });
    }
}

export class IntentRuleDelete extends CustomEvent<RuleEventDetail> {
    constructor(groupId: string, rule: TabGroupRule) {
        super(EventMap.IntentRuleDelete, {
            bubbles: true,
            detail: {groupId, rule},
        });
    }
}

export class IntentRuleEdit extends CustomEvent<RuleEventDetail> {
    constructor(public groupId: string, public rule: TabGroupRule) {
        super(EventMap.IntentRuleEdit, {
            bubbles: true,
            detail: {groupId, rule},
        });
    }
}

export class IntentImportExport extends Event {
    constructor() {
        super(EventMap.IntentImportExport, {bubbles: true});
    }
}

export class IntentToggleOverride extends Event {
    constructor() {
        super(EventMap.IntentToggleOverride, {bubbles: true});
    }
}
