import { TabGroupRule } from '../classes/tabgroup-rule.class';
import { EventMap } from './index';

export interface RuleEventDetail {
    groupId: string;
    rule: TabGroupRule;
}

export class RuleUpdateEvent extends CustomEvent<RuleEventDetail> {
    constructor(public groupId: string, public rule: TabGroupRule) {
        super(EventMap.RuleUpdate, {
            bubbles: true,
            detail: {groupId, rule},
        });
    }
}
