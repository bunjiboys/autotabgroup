import { TabGroupRulePath } from '../classes/tabgroup-rule-path.class';
import { EventMap } from './index';

export interface PathDeleteInternalEventDetail {
    path: TabGroupRulePath;
}

export class PathDeleteInternalEvent extends CustomEvent<PathDeleteInternalEventDetail> {
    constructor(path: TabGroupRulePath) {
        super(EventMap.PathDeleteInternal1, {
            bubbles: true,
            detail: {path},
        });
    }
}
