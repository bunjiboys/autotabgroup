import { NotifyColors } from '../interfaces/colors';
import { EventMap } from './index';

export interface IAlertEvent {
    message: string;
    level?: NotifyColors;
}

export class AlertEvent extends CustomEvent<IAlertEvent> {
    constructor(public message: string, public level: NotifyColors) {
        super(EventMap.Alert, {bubbles: true, detail: {message, level}});
    }
}
