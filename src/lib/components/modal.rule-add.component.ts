import { Modal } from 'bootstrap';
import { TabGroupRulePath } from '../classes/tabgroup-rule-path.class';
import { TabGroupRule } from '../classes/tabgroup-rule.class';
import { RuleUpdateEvent } from '../events';
import { ITabGroupRuleForm } from '../interfaces/forms.interface';
import { getFormValue } from '../utils';
import { FormRuleAddPathElement } from './form.rule-path.component';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <div class="modal modal-lg fade" id="modalAddRule" tabindex="-1">
        <form id="formRuleAdd" class="needs-validation">
            <input type="hidden" id="add-rule-group-id">
            <input type="hidden" id="add-rule-mode">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Add Rule
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <section>
                            <!-- region Domain settings -->
                            <h6>Domain</h6>
                            <p>
                                Define the domain you would like to match tabs against. If the <code>Include subdomains</code> option is enabled, this will also match any
                                subdomains
                                of the provided domain. For example, using a value of <code>youtube.com</code>, with wildcard enabled, would match <code>youtube.com</code>,
                                <code>www.youtube.com</code>, and any other subdomains of <code>youtube.com</code>.
                            </p>

                            <div class="form-floating mb-3">
                                <input
                                    type="text"
                                    class="form-control"
                                    name="domain"
                                    id="domain"
                                    pattern="(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]"
                                    placeholder="Domain"
                                    required>
                                <label for="domain" class="form-label">Domain</label>
                            </div>

                            <div class="form-check form-switch">
                                <input
                                    type="checkbox"
                                    class="form-check-input"
                                    role="switch"
                                    id="includeWildcard"
                                    name="includeWildcard"
                                    checked>
                                <label for="includeWildcard" class="form-check-label">Include subdomains (*.domain.tld)</label>
                            </div>
                            <!-- endregion -->
                        </section>

                        <hr>

                        <section>
                            <!-- Path settings -->
                            <h6>
                                Paths
                            </h6>

                            <p>
                                This optional section allows you to define a rule that only matches specific paths on a domain. If no paths
                                are defined, any tab that matches the domain will be grouped in to this tabgroup.
                            </p>

                            <div>
                                <button type="button" id="btnAddPath" class="btn btn-outline-success mb-4">Add Path</button>
                            </div>

                            <div id="rule-list"></div>
                        </section>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-warning" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success" disabled>Add</button>
                    </div>
                </div>
            </div>
        </form>
    </div>`;

export class ModalRuleAddElement extends HTMLElement {
    private _domain = '';
    private _groupId = '';
    private _rule: TabGroupRule | null = null;
    private _title = 'Add Rule';
    private _button = 'Add';
    private _mode: 'add' | 'edit' = 'add';
    private _idx = 0;

    // region Getters & Setters
    get groupId(): string {
        return this._groupId;
    }

    set groupId(value: string) {
        this._groupId = value;
    }

    get rule(): TabGroupRule | null {
        return this._rule;
    }

    set rule(value: TabGroupRule | null) {
        this._rule = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get mode(): 'add' | 'edit' {
        return this._mode;
    }

    set mode(value: 'add' | 'edit') {
        this._mode = value;
    }

    get domain(): string {
        return this._domain || '';
    }

    set domain(value: string) {
        this._domain = value;
    }

    // endregion

    connectedCallback() {
        this.setupDom();
        if (this.mode === 'edit') {
            this.setupDefaults();
        }
    }

    private setupDefaults() {
        const title = this.querySelector<HTMLElement>('.modal-title');
        const button = this.querySelector<HTMLButtonElement>('button[type="submit"]');
        const domain = this.querySelector<HTMLInputElement>('#domain');
        const wildcard = this.querySelector<HTMLInputElement>('#includeWildcard');
        const rulelist = this.querySelector<HTMLDivElement>('#rule-list');
        const btnAddPath = this.querySelector<HTMLButtonElement>('#btnAddPath');

        if (domain && wildcard && rulelist && title && button && this.rule && this.groupId && btnAddPath) {
            title.innerText = 'Edit Rule';
            button.innerText = 'Edit';
            domain.value = this.rule.domain;
            wildcard.checked = this.rule.includeWildcard;

            for (let i = 0; i < this.rule.paths.length; i++) {
                const pathEl = new FormRuleAddPathElement();
                pathEl.path = this.rule.paths[i];
                pathEl.index = i;

                rulelist.appendChild(pathEl);
                this._idx = i;
            }
        }
    }

    private setupDom() {
        const clone = document.importNode(template.content, true);
        const modalEl = clone.querySelector<HTMLDivElement>('.modal');
        const formEl = clone.querySelector<HTMLFormElement>('form');
        const rulelist = clone.querySelector<HTMLDivElement>('#rule-list');
        const btnAddPath = clone.querySelector<HTMLButtonElement>('#btnAddPath');
        const btnSubmit = clone.querySelector<HTMLButtonElement>('button[type="submit"]');

        this.appendChild(clone);

        if (rulelist && btnAddPath) {
            btnAddPath.addEventListener('click', evt => {
                evt.preventDefault();
                evt.stopPropagation();

                const pathEl = new FormRuleAddPathElement();
                pathEl.index = ++this._idx;
                rulelist.appendChild(pathEl);
            });
        }

        if (modalEl && formEl && btnSubmit) {
            btnSubmit.addEventListener('click', evt => {
                evt.preventDefault();
                evt.stopPropagation();

                const formdata = getFormValue<ITabGroupRuleForm>(formEl);
                if (formdata) {
                    for (const [k, v] of Object.entries(formdata)) {
                        if ((k.startsWith('recursive') || k === 'includeWildcard') && v === 'on') {
                            formdata[k] = true;
                        }
                    }
                }

                const paths: TabGroupRulePath[] = [];
                if (formdata) {
                    for (let i = 0; i < 10; i++) {
                        const pathKey = `path${i}`;
                        const recursiveKey = `recursive${i}`;

                        if (pathKey in formdata && formdata[pathKey]) {
                            const recursive = recursiveKey in formdata;
                            paths.push(new TabGroupRulePath(formdata[pathKey] as string, recursive));
                        }
                    }
                    const rule = new TabGroupRule(formdata.domain, paths, formdata.includeWildcard);
                    this.dispatchEvent(new RuleUpdateEvent(this.groupId, rule));
                }

                const modal = Modal.getInstance('#modalAddRule');
                if (modal) {
                    modal.hide();
                }
            });
        }
    }
}
