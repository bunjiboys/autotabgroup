import { TabGroup } from '../classes/tabgroup.class';
import { TabGroupElement } from './tabgroup.component';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <div class="container p-2">
        <div class="row py-2 header">
            <div class="col col-name">Name</div>
            <div class="col col-color">Color</div>
            <div class="col col-rules">Rules</div>
            <div class="col col-actions">&nbsp;</div>
        </div>
    </div>`;

export class TabGroupListElement extends HTMLElement {
    static TAG = 'atg-tabgroup-list';

    private _tabGroups: TabGroup[] = [];

    // region Getters & Setters
    get tabGroups(): TabGroup[] {
        return this._tabGroups;
    }

    set tabGroups(value: TabGroup[]) {
        this._tabGroups = value;
    }

    // endregion

    connectedCallback() {
        const parent = document.importNode(template.content, true);
        const container = parent.querySelector<HTMLDivElement>('div.container');

        if (container) {
            const row = document.createElement('div');
            row.classList.add('row');

            if (this.tabGroups.length) {
                for (const tabGroup of this.tabGroups) {
                    const group = new TabGroupElement();
                    group.tabGroup = tabGroup;
                    container.appendChild(group);

                    row.appendChild(group);
                }
            } else {
                const noGroupMsg = document.createElement('div');
                noGroupMsg.classList.add('hint', 'mt-4');
                noGroupMsg.innerText = 'To get started, click the "Create Group" button below and follow the instructions';

                row.appendChild(noGroupMsg);
            }

            container.appendChild(row);
        }

        this.appendChild(parent);
    }
}
