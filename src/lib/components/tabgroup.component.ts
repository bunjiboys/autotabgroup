import { TabGroup } from '../classes/tabgroup.class';
import { RuleListActionsElement } from './rule-list-actions.component';
import { RuleListElement } from './rule-list.component';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <div class="row py-2">
        <div class="col col-name">WoW</div>
        <div class="col col-color">
            <span class="dot selected">&nbsp;</span>
        </div>
        <div class="col col-rules"></div>
        <div class="col col-actions"></div>
    </div>`;

export class TabGroupElement extends HTMLElement {
    private _tabGroup: TabGroup | null = null;

    // region Getters & Setters
    get tabGroup(): TabGroup | null {
        return this._tabGroup;
    }

    set tabGroup(value: TabGroup | null) {
        this._tabGroup = value;
    }

    // endregion

    connectedCallback() {
        if (this.tabGroup) {
            const clone = document.importNode(template.content, true);
            const name = clone.querySelector<HTMLTableCellElement>('.col-name');
            const color = clone.querySelector<HTMLTableCellElement>('.col-color .dot');
            const rulesEl = clone.querySelector<HTMLDivElement>('.col-rules');
            const actionsEl = clone.querySelector<RuleListActionsElement>('.col-actions');

            if (this.tabGroup && name && color && rulesEl && actionsEl) {
                name.innerText = this.tabGroup.name;
                color.classList.add(this.tabGroup.color);

                const rules = new RuleListElement();
                rules.rules = this.tabGroup.rules;
                rules.groupId = this.tabGroup.id;
                rulesEl.appendChild(rules);

                const actions = new RuleListActionsElement();
                actions.groupId = this.tabGroup.id;
                actionsEl.appendChild(actions);

                this.appendChild(clone);
            }
        }
    }
}
