import { TabGroupRulePath } from '../classes/tabgroup-rule-path.class';
import { PathDeleteInternalEvent } from '../events/paths.events';

export class PathListElement extends HTMLElement {
    private _paths: TabGroupRulePath[] = [];

    get paths(): TabGroupRulePath[] {
        return this._paths;
    }

    set paths(value: TabGroupRulePath[]) {
        this._paths = value;
    }

    connectedCallback() {
        if (this.paths.length) {
            const ul = document.createElement('ul');

            for (const path of this.paths) {
                const li = document.createElement('li');
                li.innerText = path.pattern;

                const lnkDel: SVGSVGElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
                lnkDel.setAttribute('fill', 'currentColor');
                lnkDel.setAttribute('width', '16');
                lnkDel.setAttribute('height', '16');
                lnkDel.setAttribute('viewBox', '0 0 16 16');

                const spath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
                spath.setAttribute('d', 'M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z');
                lnkDel.appendChild(spath);

                //lnkDel.classList.add('bi', 'bi-trash3-fill', 'rule-actions', 'text-danger');
                lnkDel.addEventListener('click', (evt) => {
                    evt.preventDefault();
                    evt.stopPropagation();

                    this.dispatchEvent(new PathDeleteInternalEvent(path));
                });
                li.appendChild(lnkDel);

                ul.appendChild(li);
            }

            this.appendChild(ul);
        } else {
            const span = document.createElement('span');
            span.style.fontStyle = 'italic';
            span.innerText = 'Entire Domain';
            this.appendChild(span);
        }
    }
}
