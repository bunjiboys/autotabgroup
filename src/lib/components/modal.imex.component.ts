import { Tab } from 'bootstrap';
import { Configuration, ConfigurationError, getConfig } from '../classes/configuration.class';
import { AlertEvent } from '../events';
import { getModal } from '../utils';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <div class="modal modal-lg fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header mb-0 pb-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a id="import-tab" class="nav-link active" data-bs-toggle="tab" data-bs-target="#import" aria-current="page"
                               href="#">Import</a>
                        </li>
                        <li class="nav-item">
                            <a id="export-tab" class="nav-link" data-bs-toggle="tab" data-bs-target="#export" href="#">Export</a>
                        </li>
                    </ul>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="tab-content">
                        <div id="import" class="tab-pane fade show active" role="tabpanel">
                            <section>
                                <p>
                                    Drop a file on the field below or paste the contents into the box to import a previously exported
                                    configuration.
                                </p>
                                <p>
                                    <b class="text-danger">Note:</b> Any existing tab groups you already have configured will be removed
                                    when importing a configuration
                                </p>
                            </section>

                            <section>
                                <textarea data-atg-imex-import class="form-control" spellcheck="false"></textarea>
                                <div class="imex-overlay">
                                    <div class="message">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="currentColor" viewBox="0 0 16 16">
                                                <path
                                                    d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                                <path
                                                    d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z"/>
                                            </svg>
                                        </div>
                                        <div class="text">
                                            Drop here
                                        </div>
                                    </div>
                                </div>
                                <iframe src="/sandbox/sandbox.html" style="display: none"></iframe>
                            </section>

                            <section>
                                <button type="button" class="btn btn-success" data-atg-import disabled>Import</button>
                            </section>
                        </div>

                        <div id="export" class="tab-pane fade" role="tabpanel">
                            <section>
                                <p>
                                    Below you'll find a copy of your current tab group configurations. Click the "Copy to Clipboard" button
                                    below to copy the configuration, which can then be saved as a text file or shared with others.
                                </p>
                                <textarea data-atg-imex-export class="form-control" readonly spellcheck="false"></textarea>
                            </section>
                            <section>
                                <button type="button" class="btn btn-success" data-atg-export-download>Download</button>
                                <button type="button" class="btn btn-info" data-atg-export-clip>Copy to Clipboard</button>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>`;

export class ModalImexElement extends HTMLElement {
    private readonly _config: Configuration;

    constructor() {
        super();
        this._config = getConfig();
    }

    connectedCallback() {
        const clone = document.importNode(template.content, true);
        const modalEl = clone.querySelector<HTMLDivElement>('.modal');
        const triggerTabList = clone.querySelectorAll<HTMLDivElement>('[data-bs-toggle="tab"][data-bs-target]');

        if (modalEl) {
            modalEl.addEventListener('shown.bs.modal', () => {
                for (const triggerEl of Array.from(triggerTabList)) {
                    const tabTrigger = new Tab(triggerEl);

                    if (triggerEl.classList.contains('active')) {
                        tabTrigger.show();
                    }

                    triggerEl.addEventListener('click', (event) => {
                        event.preventDefault();
                        tabTrigger.show();
                    });
                }
                this.setupButtons();
                this.setupDragDrop();

                const textarea = this.querySelector<HTMLTextAreaElement>('[data-atg-imex-export]');
                if (textarea) {
                    textarea.value = JSON.stringify(this._config.serialize(), null, 4);
                }
            });
        }

        this.appendChild(clone);
    }

    private setupDragDrop() {
        const textarea = this.querySelector<HTMLTextAreaElement>('[data-atg-imex-import]');
        const overlay = this.querySelector<HTMLDivElement>('[data-atg-imex-import] + .imex-overlay');
        const btnImport = this.querySelector<HTMLButtonElement>('[data-atg-import]');

        if (textarea && overlay && btnImport) {
            window.addEventListener('message', (evt) => {
                btnImport.disabled = !evt.data.valid;

                if (evt.data.valid) {
                    textarea.classList.remove('invalid');
                    textarea.classList.add('valid');
                    btnImport.classList.replace('btn-danger', 'btn-success');
                } else {
                    textarea.classList.remove('valid');
                    textarea.classList.add('invalid');
                    btnImport.classList.replace('btn-success', 'btn-danger');
                }
            });

            textarea.addEventListener('input', () => {
                const data = textarea.value.trim();
                document.querySelector<HTMLIFrameElement>('iframe')?.contentWindow?.postMessage({command: 'validate', context: {data}}, '*');
            });

            textarea.addEventListener('dragover', () => overlay.style.display = 'grid');
            textarea.addEventListener('dragleave', () => overlay.style.display = 'none');
            textarea.addEventListener('drop', (evt: DragEvent) => {
                evt.preventDefault();

                if (evt.dataTransfer && evt.dataTransfer.items) {
                    for (const item of evt.dataTransfer.items) {
                        item.getAsFile()?.text().then(data => {
                            try {
                                const parsed = JSON.parse(data);
                                document.querySelector<HTMLIFrameElement>('iframe')?.contentWindow?.postMessage({command: 'validate', context: {data: parsed}}, '*');
                                textarea.value = data;
                            } catch (ex) {
                                if (ex instanceof ConfigurationError) {
                                    this.dispatchEvent(new AlertEvent(ex.message, ex.color));
                                }
                            }
                        });
                    }
                }

                overlay.style.display = 'none';
            });

            const pos = textarea.getClientRects()[0];
            overlay.style.width = `${pos.width}px`;
            overlay.style.height = `${pos.height}px`;
            overlay.style.top = `${textarea.offsetTop}px`;
            overlay.style.left = `${textarea.offsetLeft}px`;
        }
    }

    private setupButtons() {
        const btnImport = this.querySelector<HTMLButtonElement>('[data-atg-import]');
        const btnExportClip = this.querySelector<HTMLButtonElement>('[data-atg-export-clip]');
        const btnExportDownload = this.querySelector<HTMLButtonElement>('[data-atg-export-download]');

        btnImport?.addEventListener('click', (evt) => {
            evt.preventDefault();
            evt.stopPropagation();

            const modalEl = this.querySelector<HTMLDivElement>('.modal');
            const textarea = this.querySelector<HTMLTextAreaElement>('[data-atg-imex-import]');
            if (modalEl && textarea) {
                const value = textarea.value.trim();
                if (value) {
                    try {
                        this._config.importConfiguration(value);
                        getModal(modalEl)?.hide();
                    } catch (ex) {
                        if (ex instanceof ConfigurationError) {
                            this.dispatchEvent(new AlertEvent(ex.message, ex.color));
                            return;
                        }
                    }
                } else {
                    this.dispatchEvent(new AlertEvent('Configuration cannot be empty', 'danger'));
                }
            }
        });

        const exportTextarea = this.querySelector<HTMLTextAreaElement>('[data-atg-imex-export]');
        if (exportTextarea) {
            const modalEl = this.querySelector<HTMLDivElement>('.modal');
            if (btnExportDownload && btnExportClip && modalEl) {
                const modal = getModal(modalEl);

                btnExportClip.addEventListener('click', (evt) => {
                    evt.preventDefault();
                    evt.stopPropagation();

                    navigator.clipboard.writeText(exportTextarea.value).then(
                        () => {
                            this.dispatchEvent(new AlertEvent('Configuration copied to clipboard', 'success'));
                            modal.hide();
                        },
                    );
                });

                btnExportDownload?.addEventListener('click', (evt) => {
                    evt.preventDefault();
                    evt.stopPropagation();

                    const dl = document.createElement('a') as HTMLAnchorElement;
                    dl.href = `data:text/plain;base64,${window.btoa(exportTextarea.value)}`;
                    dl.download = 'autotabgroup.config.json';
                    dl.click();

                    modal.hide();
                });
            }
        }
    }
}
