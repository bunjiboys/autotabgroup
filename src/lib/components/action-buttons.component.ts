import { IntentGroupAdd, IntentImportExport } from '../events';
import { IntentToggleOverride } from '../events/intents.events';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <button data-atg-action="addGroup" class="btn btn-primary">Create Group</button>
    <button data-atg-action="importExport" class="btn btn-success">Import/Export Config</button>
    <button data-atg-action="toggleOverride" class="btn btn-secondary">Override Groups</button>
`;

export class ActionButtonsComponent extends HTMLElement {
    connectedCallback() {
        const clone = document.importNode(template.content, true);
        const btnAddGroup = clone.querySelector<HTMLButtonElement>('[data-atg-action="addGroup"]');
        const btnImEx = clone.querySelector<HTMLButtonElement>('[data-atg-action="importExport"]');
        const btnOverride = clone.querySelector<HTMLButtonElement>('[data-atg-action="toggleOverride"]');

        if (btnAddGroup && btnImEx && btnOverride) {
            btnAddGroup.addEventListener('click', (evt) => {
                evt.preventDefault();
                evt.stopPropagation();

                this.dispatchEvent(new IntentGroupAdd());
            });

            btnImEx.addEventListener('click', (evt) => {
                evt.preventDefault();
                evt.stopPropagation();

                this.dispatchEvent(new IntentImportExport());
            });

            btnOverride.addEventListener('click', (evt) => {
                evt.preventDefault();
                evt.stopPropagation();

                this.dispatchEvent(new IntentToggleOverride());
            });
        }

        this.appendChild(clone);
    }
}
