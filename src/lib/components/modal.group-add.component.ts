import { Modal } from 'bootstrap';
import { AlertEvent, GroupAddEvent } from '../events';
import { ITabGroupForm } from '../interfaces/forms.interface';
import { getFormValue } from '../utils';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <div class="modal modal-lg fade" id="modalAddGroup" tabindex="-1">
        <form id="formGroupAdd" class="needs-validation">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title fs-5" id="grpmdlTitle">
                            Add Group
                        </h6>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <div class="form-floating mb-3">
                                <input
                                    type="text"
                                    class="form-control"
                                    name="name"
                                    id="name"
                                    pattern="^[a-zA-Z0-9\\.,\\|/]+$"
                                    placeholder="Group Name"
                                    required>
                                <label for="name" class="form-label">Group Name</label>
                            </div>
                            <div class="form-floating mb-3">
                                <select id="color" name="color" class="form-control form-select form-select-lg blue" aria-label="Select a color" required>
                                    <option class="blue" value="blue">Blue</option>
                                    <option class="cyan" value="cyan">Cyan</option>
                                    <option class="green" value="green">Green</option>
                                    <option class="grey" value="grey">Grey</option>
                                    <option class="pink" value="pink">Pink</option>
                                    <option class="purple" value="purple">Purple</option>
                                    <option class="red" value="red">Red</option>
                                    <option class="yellow" value="yellow">Yellow</option>
                                </select>
                                <label for="color" class="form-label">Color</label>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-warning" data-bs-dismiss="modal">Cancel</button>
                        <button id="btnCreateTabGroup" type="submit" class="btn btn-success" disabled>Create</button>
                    </div>
                </div>
            </div>
        </form>
    </div>`;

export class ModalGroupAddElement extends HTMLElement {
    connectedCallback() {
        const clone = document.importNode(template.content, true);

        const btnSubmit = clone.querySelector<HTMLButtonElement>('button[type="submit"]');
        btnSubmit?.addEventListener('click', evt => {
            evt.preventDefault();
            evt.stopPropagation();

            const form = document.querySelector<HTMLFormElement>('#formGroupAdd');
            if (form) {
                if (!form.checkValidity()) {
                    const errors = Array.from(form.querySelectorAll<HTMLInputElement | HTMLSelectElement>('[name]')).map(el => el.validationMessage).filter(String);
                    this.dispatchEvent(new AlertEvent(`Unable to add group:<br>${errors.join('<br>')}`, 'warning'));

                    return;
                }

                const formdata = getFormValue<ITabGroupForm>(form);
                if (formdata) {
                    this.dispatchEvent(new GroupAddEvent(formdata.name, formdata.color));
                }

                const modal = Modal.getInstance('#modalAddGroup');
                if (modal) {
                    modal.hide();
                }
            }
        });

        const select = clone.querySelector<HTMLSelectElement>('select');
        if (select) {
            select.addEventListener('input', () => {
                const options = Array.from(document.querySelectorAll('option')).map(opt => opt.value);
                select.classList.remove(...options);
                select.classList.add(select.value);
            });
        }

        this.appendChild(clone);
    }
}
