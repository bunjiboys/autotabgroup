import { ConfirmEvent } from '../events';
import { IModalConfirmContext } from '../events/confirm.events';
import { getModal } from '../utils';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <div class="modal modal-lg fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header mb-0 pb-0">
                    <h5 data-atg-confirm-title></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" data-atg-confirm-body></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-atg-confirm-no>No</button>
                    <button type="button" class="btn btn-success" data-atg-confirm-yes>Yes</button>
                </div>
            </div>
        </div>
    </div>`;

export class ModalConfirmElement extends HTMLElement {
    private _modalId: string | null = null;
    private _modalTitle: string | null = null;
    private _modalBody: HTMLElement | string | null = null;
    private _noButton = 'No';
    private _yesButton = 'Yes';
    private _context: IModalConfirmContext | null = null;

    // region Getters & Setters
    get modalId(): string | null {
        return this._modalId;
    }

    set modalId(value: string | null) {
        this._modalId = value;
    }

    get modalTitle(): string | null {
        return this._modalTitle;
    }

    set modalTitle(value: string | null) {
        this._modalTitle = value;
    }

    get modalBody(): HTMLElement | string | null {
        return this._modalBody;
    }

    set modalBody(value: HTMLElement | string | null) {
        this._modalBody = value;
    }

    get noButton(): string {
        return this._noButton;
    }

    set noButton(value: string) {
        this._noButton = value;
    }

    get yesButton(): string {
        return this._yesButton;
    }

    set yesButton(value: string) {
        this._yesButton = value;
    }

    get context(): IModalConfirmContext | null {
        return this._context;
    }

    set context(value: IModalConfirmContext | null) {
        console.log('new context', value);
        this._context = value;
    }

    // endregion

    connectedCallback() {
        const clone = document.importNode(template.content, true);
        this.appendChild(clone);

        console.log(this.context);

        const title = this.querySelector<HTMLSpanElement>('[data-atg-confirm-title]');
        const body = this.querySelector<HTMLDivElement>('[data-atg-confirm-body]');
        const btnNo = this.querySelector<HTMLButtonElement>('button[data-atg-confirm-no]');
        const btnYes = this.querySelector<HTMLButtonElement>('button[data-atg-confirm-yes]');

        if (this.modalId && this.modalBody && this.modalTitle && title && body && btnNo && btnYes) {
            if (this.modalBody instanceof HTMLElement) {
                body.appendChild(this.modalBody);
            } else {
                body.innerText = this.modalBody;
            }

            title.innerText = this.modalTitle;
            btnNo.innerText = this.noButton;
            btnYes.innerText = this.yesButton;

            const modalEl = this.querySelector<HTMLDivElement>('.modal');
            if (modalEl) {
                const modal = getModal(modalEl);
                if (modal) {
                    btnNo.addEventListener('click', () => {
                        modal.hide();
                    });

                    btnYes.addEventListener('click', (evt) => {
                        evt.preventDefault();
                        evt.stopPropagation();

                        if (this.modalId) {
                            this.dispatchEvent(new ConfirmEvent(true, this.modalId, this.context));
                        }

                        modal.hide();
                    });
                }
            }
        }
    }
}
