import { Alert } from 'bootstrap';
import { NotifyColors } from '../interfaces/colors';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <div class="alert alert-dismissible fade show" role="alert">
        <div class="alert-body"></div>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>`;

export class AlertsElement extends HTMLElement {
    private _container?: HTMLDivElement;

    constructor() {
        super();

    }

    connectedCallback() {
        this._container = document.createElement('div');
        this._container.classList.add('alert-list');
        this.appendChild(this._container);
    }

    notify(message: string, color: NotifyColors) {
        if (this.isConnected && this._container) {
            const clone = document.importNode(template.content, true);

            const alert = clone.querySelector<HTMLDivElement>('.alert');
            const body = clone.querySelector<HTMLDivElement>('.alert-body');

            if (alert && body) {
                const alertId = `alert-${Math.round(Math.random() * 1_000_000) + Date.now()}`;
                alert.classList.add(`alert-${color}`);
                alert.id = alertId;
                body.innerHTML = message;

                alert.addEventListener('closed.bs.alert', evt => {
                    evt.preventDefault();
                    evt.stopPropagation();

                    const alertEl = this._container?.querySelector<HTMLDivElement>(`#${alertId}`);
                    if (alertEl) {
                        this._container?.removeChild(alertEl);
                    }
                });

                setTimeout(() => {
                    const alertEl = document.querySelector<HTMLDivElement>(`#${alertId}`);
                    if (alertEl) {
                        Alert.getOrCreateInstance(alertEl)?.close();
                    }
                }, 5000);
            }

            this._container.appendChild(clone);
        }
    }
}
