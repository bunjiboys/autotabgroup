import { IntentGroupDelete, IntentRuleAdd } from '../events';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-success btn-sm atg-add">Add Rule</button>
        <button type="button" class="btn btn-outline-danger btn-sm atg-delete">Delete Group</button>
    </div>`;

export class RuleListActionsElement extends HTMLElement {
    private _groupId = '';

    // region Getters & Setters
    get groupId(): string {
        return this._groupId;
    }

    set groupId(value: string) {
        this._groupId = value;
    }

    // endregion

    connectedCallback() {
        const clone = document.importNode(template.content, true);
        const btnSuccess = clone.querySelector<HTMLButtonElement>('.atg-add');
        const btnDelete = clone.querySelector<HTMLButtonElement>('.atg-delete');

        if (this.groupId && clone) {
            if (btnSuccess) {
                btnSuccess.addEventListener('click', (evt) => {
                    evt.preventDefault();
                    evt.stopPropagation();

                    this.dispatchEvent(new IntentRuleAdd(this.groupId));
                });
            }

            if (btnDelete) {
                btnDelete?.addEventListener('click', (evt) => {
                    evt.preventDefault();
                    evt.stopPropagation();

                    btnDelete.dispatchEvent(new IntentGroupDelete(this.groupId));
                });
            }
        }
        this.appendChild(clone);
    }
}
