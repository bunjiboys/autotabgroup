import { TabGroupRulePath } from '../classes/tabgroup-rule-path.class';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <div class="path-grid">
        <div class="form-floating mb-3">
            <input type="text" class="form-control" name="" id="" placeholder="Path" required>
            <label class="form-label" for="">Path</label>
        </div>
        <div class="form-check form-switch">
            <input type="checkbox" class="form-check-input" role="switch" id="" name="" checked>
            <label class="form-check-label" for="">Recursive matching</label>
        </div>
        <div class="path-delete">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16" class="delete">
                <path
                    d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
            </svg>
        </div>
    </div>`;

export class FormRuleAddPathElement extends HTMLElement {
    private _path: TabGroupRulePath | null = null;
    private _index = 0;

    get path(): TabGroupRulePath | null {
        return this._path;
    }

    set path(value: TabGroupRulePath | null) {
        this._path = value;
    }

    get index(): number {
        return this._index;
    }

    set index(value: number) {
        this._index = value;
    }

    connectedCallback() {
        const clone = document.importNode(template.content, true);
        const path = clone.querySelector<HTMLInputElement>('.form-control');
        const lblPath = clone.querySelector<HTMLLabelElement>('.form-floating > label');
        const recursive = clone.querySelector<HTMLInputElement>('.form-check-input[role="switch"]');
        const lblRecursive = clone.querySelector<HTMLLabelElement>('.form-check > label');
        const lnkDelete = clone.querySelector<HTMLElement>('.path-delete svg');

        if (path && recursive && lblPath && lblRecursive && lnkDelete) {
            const pid = `path${this.index}`;
            const rid = `recursive${this.index}`;
            path.id = pid;
            path.name = pid;
            lblPath.htmlFor = pid;
            recursive.id = rid;
            recursive.name = rid;
            lblRecursive.htmlFor = rid;

            if (this.path?.path) {
                path.value = this.path.path;
            }

            if (this.path?.recursive) {
                recursive.checked = true;
            }

            lnkDelete.addEventListener('click', (evt) => {
                evt.preventDefault();
                evt.stopPropagation();

                if (this.parentElement) {
                    this.parentElement.removeChild(this);
                }
            });
        }

        this.appendChild(clone);
    }
}
