import { TabGroupRule } from '../classes/tabgroup-rule.class';
import { EventMap, IntentPathDelete, IntentRuleDelete, IntentRuleEdit } from '../events';
import { PathDeleteInternalEvent } from '../events/paths.events';
import { PathListElement } from './path-list.components';

const template = document.createElement('template');
//language=HTML
template.innerHTML = `
    <div class="row">
        <div class="col">
            <span class="atg-rule-name"></span>
            <div class="rule-actions">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16" class="edit">
                    <path
                        d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fill-rule="evenodd"
                          d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                </svg>

                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16" class="delete">
                    <path
                        d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
                </svg>
            </div>
        </div>
        <div class="col" data-atg-path-list>
        </div>
    </div>`;

export class RuleListElement extends HTMLElement {
    private _groupId = '';
    private _rules: TabGroupRule[] = [];

    // region Getters & Setters
    get groupId(): string {
        return this._groupId;
    }

    set groupId(value: string) {
        this._groupId = value;
    }

    get rules(): TabGroupRule[] {
        return this._rules;
    }

    set rules(value: TabGroupRule[]) {
        this._rules = value;
    }

    // endregion

    connectedCallback() {
        if (this._rules.length) {
            const container = document.createElement('div');
            container.classList.add('container', 'p-0', 'm-0');

            for (const rule of this._rules) {
                const clone = document.importNode(template.content, true);
                const lnkEdit = clone.querySelector<HTMLElement>('.edit');
                const lnkDelete = clone.querySelector<HTMLElement>('.delete');
                const name = clone.querySelector<HTMLSpanElement>('.atg-rule-name');
                const pathsEl = clone.querySelector<HTMLDivElement>('[data-atg-path-list]');

                if (lnkEdit && lnkDelete && name && pathsEl) {
                    lnkEdit.addEventListener('click', (evt) => {
                        evt.preventDefault();
                        evt.stopPropagation();

                        this.dispatchEvent(new IntentRuleEdit(this._groupId, rule));
                    });

                    lnkDelete.addEventListener('click', (evt) => {
                        evt.preventDefault();
                        evt.stopPropagation();

                        this.dispatchEvent(new IntentRuleDelete(this._groupId, rule));
                    });

                    name.innerHTML = rule.toString();

                    const paths = new PathListElement();
                    paths.paths = rule.paths;
                    paths.addEventListener(EventMap.PathDeleteInternal1, (evt: PathDeleteInternalEvent) => {
                        evt.preventDefault();
                        evt.stopPropagation();

                        this.dispatchEvent(new IntentPathDelete(this._groupId, rule, evt.detail.path));
                    });
                    pathsEl.appendChild(paths);
                }

                container.appendChild(clone);
            }

            this.appendChild(container);
        } else {
            const msg = document.createElement('span');
            msg.style.fontStyle = 'italic';

            msg.innerText = 'No rules defined';
            this.appendChild(msg);
        }
    }
}
