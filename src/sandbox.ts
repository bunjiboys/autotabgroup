import Ajv2020 from 'ajv/dist/2020';

export const validateConfigurationSchema = new Ajv2020().compile({
    $schema: 'https://json-schema.org/draft/2020-12/schema',
    $id: 'https://gitlab.com/bunjiboys/autotabgroup/configuration.schema.json',
    title: 'Configuration',
    description: 'AutoTabGroup configuration file',
    type: 'object',
    properties: {
        overrideGroup: {
            description: 'Override existing tab group association when navigating',
            type: 'boolean',
        },
        tabGroups: {
            type: 'array',
            items: {
                $ref: '/schemas/tabgroup',
            },
            minItems: 0,
        },
    },
    required: [
        'overrideGroup',
        'tabGroups',
    ],

    $defs: {
        tabgroup: {
            $id: '/schemas/tabgroup',
            $schema: 'https://json-schema.org/draft/2020-12/schema',
            type: 'object',
            properties: {
                id: {
                    description: 'Unique Group ID',
                    type: 'string',
                },
                name: {
                    description: 'TabGroup name',
                    type: 'string',
                },
                color: {
                    description: 'TabGroup color',
                    type: 'string',
                    pattern: '(grey|blue|red|yellow|green|pink|purple|cyan|orange)',
                },
                rules: {
                    type: 'array',
                    items: {
                        $ref: '/schemas/rule',
                    },
                    minItems: 0,
                },
            },
            required: [
                'id',
                'name',
                'color',
                'rules',
            ],
        },
        rule: {
            $id: '/schemas/rule',
            $schema: 'https://json-schema.org/draft/2020-12/schema',
            type: 'object',
            properties: {
                domain: {
                    description: 'Domain to match',
                    type: 'string',
                },
                includeWildcard: {
                    description: 'Also match direct sub-domains',
                    type: 'boolean',
                },
                paths: {
                    description: 'List of paths to match, optional',
                    type: 'array',
                    items: {
                        $ref: '/schemas/path',
                    },
                    minItems: 0,
                },
            },
            required: [
                'domain',
                'includeWildcard',
                'paths',
            ],
        },
        path: {
            $id: '/schemas/path',
            $schema: 'https://json-schema.org/draft/2020-12/schema',
            type: 'object',
            properties: {
                path: {
                    description: 'Path to match',
                    type: 'string',
                },
                recursive: {
                    description: 'Apply a recursive match on the path',
                    type: 'boolean',
                },
            },
            required: [
                'path',
                'recursive',
            ],
        },
    },
});

(() => {
    window.addEventListener('message', (evt) => {
        const command = evt.data.command;
        const wp = evt.source as WindowProxy;

        try {
            const data = typeof evt.data.context.data === 'string' ? JSON.parse(evt.data.context.data) : evt.data.context.data;


            if (wp) {
                if (command === 'validate') {
                    const result = validateConfigurationSchema(data);
                    wp.postMessage({valid: result}, evt.origin);
                }
            }
        } catch (ex) {
            wp.postMessage({valid: false}, evt.origin);
        }
    });
})();
