import { Subject } from 'rxjs';
import { Configuration, getConfig } from './lib/classes/configuration.class';
import { TabGroup } from './lib/classes/tabgroup.class';

interface TabEvent {
    tabId: number;
    tabGroup: TabGroup;
}

const IS_DEV = !('update_url' in chrome.runtime.getManifest());
const config: Configuration = getConfig();
const sub = new Subject<TabEvent>();

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function debugLog(message: any) {
    if (IS_DEV) {
        console.log(message);
    }
}

sub.subscribe((evt: TabEvent) => {
    chrome.tabGroups.query({title: evt.tabGroup.name}, function (groupList) {
        if (groupList.length === 0) {
            debugLog('Group did not exist, creating it with this as the first window');

            chrome.tabs.group({tabIds: evt.tabId}, function (groupId) {
                chrome.tabGroups.update(groupId, {color: evt.tabGroup.color, title: evt.tabGroup.name}, (group) => {
                    debugLog(`Updated group title to ${group.title}, and color to ${group.color}`);
                });
            });

        } else if (groupList.length === 1) {
            const groupData = groupList[0];

            chrome.tabs.get(evt.tabId, (tab) => {
                if (tab.groupId !== groupData.id) {
                    chrome.tabs.group({groupId: groupData.id, tabIds: evt.tabId}, () => {
                        debugLog(`Added tab ${tab.title} to ${groupData.title}`);
                    });
                }
            });
        } else {
            console.warn('Multiple groups with the same name, cannot move tab(s)');
        }
    });
});

chrome.webNavigation.onCompleted.addListener((details) => {
    if (!config.tabGroups) {
        return;
    }

    /**
     * If a page has multiple embedded resources (YouTube videos, social media widgets etc.) an event is fired for each resource. However,
     * the parent (originating) tab will always have a frameId of 0. So to avoid grouping a tab into the wrong group because they loaded
     * a subresource, we simply just ignore any events where the frameId is not 0
     */
    if (details.frameId !== 0) {
        return;
    }

    for (const tabGroup of config.tabGroupList) {
        if (tabGroup.matches(details.url)) {
            if (!config.overrideGroup) {
                chrome.tabs.get(details.tabId).then(
                    (tab) => {
                        if (tab.groupId === -1) {
                            sub.next({tabId: details.tabId, tabGroup: tabGroup});
                        }
                    },
                );
            } else {
                sub.next({tabId: details.tabId, tabGroup: tabGroup});
            }
        }
    }
});
