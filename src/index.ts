import { AutoTabGroup } from './lib/classes/autotabgroup.class';
import { ActionButtonsComponent } from './lib/components/action-buttons.component';
import { AlertsElement } from './lib/components/alert.component';
import { FormRuleAddPathElement } from './lib/components/form.rule-path.component';
import { ModalConfirmElement } from './lib/components/modal.confirm.component';
import { ModalGroupAddElement } from './lib/components/modal.group-add.component';
import { ModalImexElement } from './lib/components/modal.imex.component';
import { ModalRuleAddElement } from './lib/components/modal.rule-add.component';
import { PathListElement } from './lib/components/path-list.components';
import { RuleListActionsElement } from './lib/components/rule-list-actions.component';
import { RuleListElement } from './lib/components/rule-list.component';
import { TabGroupListElement } from './lib/components/tabgroup-list.component';
import { TabGroupElement } from './lib/components/tabgroup.component';

(() => {
    customElements.define(TabGroupListElement.TAG, TabGroupListElement);
    customElements.define('atg-tabgroup', TabGroupElement);
    customElements.define('atg-rule-list', RuleListElement);
    customElements.define('atg-rule-list-actions', RuleListActionsElement);
    customElements.define('atg-path-list', PathListElement);
    customElements.define('atg-rule-add-path', FormRuleAddPathElement);
    customElements.define('atg-action-buttons', ActionButtonsComponent);
    customElements.define('atg-modal-confirm', ModalConfirmElement);

    // Modals
    customElements.define('atg-modal-group-add', ModalGroupAddElement);
    customElements.define('atg-modal-rule-add', ModalRuleAddElement);
    customElements.define('atg-imex', ModalImexElement);

    // Alerts
    customElements.define('atg-alerts', AlertsElement);

    const atg = new AutoTabGroup();
    atg.init();
})();
