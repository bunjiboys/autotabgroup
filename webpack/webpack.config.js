const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlMinimizerPlugin = require('html-minimizer-webpack-plugin');
const ZipPlugin = require('zip-webpack-plugin');
const {mergeWithCustomize, customizeArray} = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const commonConfig = {
    entry: {
        serviceworker: path.resolve(__dirname, '..', 'src', 'serviceworker.ts'),
        sandbox: path.resolve(__dirname, '..', 'src', 'sandbox.ts'),
        styles: path.resolve(__dirname, '..', 'src', 'css', 'index.scss'),
        index: path.resolve(__dirname, '..', 'src', 'index.ts'),
    },
    output: {
        path: path.join(__dirname, '../dist'),
        filename: '[name].js',
        clean: true,
    },
    resolve: {
        extensions: [
            '.ts',
            '.js',
            '.scss',
        ],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                options: {
                                    plugins: [
                                        [
                                            'autoprefixer'
                                        ]
                                    ]
                                }
                            }
                        }
                    },
                    {loader: 'sass-loader'},
                ],
            },
        ],
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: '.',
                    to: '.',
                    context: 'assets',
                },
            ],
        }),
    ],
};

const devConfig = {
    mode: 'development',
    devtool: 'cheap-module-source-map',
};

const prodConfig = {
    mode: 'production',
    optimization: {
        usedExports: true,
        minimize: true,
        minimizer: [
            new TerserPlugin({parallel: true}),
            new HtmlMinimizerPlugin({
                minimizerOptions: {
                    collapseWhitespace: true,
                    removeRedundantAttributes: true,
                    removeComments: true,
                    useShortDoctype: true,
                },
            }),
        ],
    },
    plugins: [
        new ZipPlugin({
            path: '../releases',
            filename: 'autotabgroup',
            fileOptions: {
                mtime: new Date(),
                mode: 0o100664,
                compress: true,
                forceZip64Format: false,
            },
        }),
    ],
};

module.exports = (env, args) => {
    switch (args.mode) {
        case 'production':
            return mergeWithCustomize({
                customizeArray: customizeArray({'plugins': 'append'}),
            })(commonConfig, prodConfig);
        case '':
        case undefined:
        case 'development':
            return mergeWithCustomize({
                customizeArray: customizeArray({'plugins': 'append'}),
            })(commonConfig, devConfig);
    }
};
