module.exports = {
    extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended'],
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint'],
    root: true,
    rules: {
        'array-callback-return': [
            'error',
            {
                checkForEach: true,
            }
        ],
        'no-constant-binary-expression': 'error',
        'no-unused-private-class-members': 'error',
        'block-scoped-var': 'error',
        'camelcase': 'error',
        'class-methods-use-this': 'error',
        'curly': 'error',
        'default-case': 'error',
        'default-case-last': 'error',
        'dot-notation': 'error',
        'eqeqeq': 'error',
        'no-eval': 'error',
        'no-empty-function': [
            'error',
            {
                allow: ['constructors'],
            }
        ],
        'no-confusing-arrow': 'error',
        'logical-assignment-operators': 'error',
        'init-declarations': 'error',
        'no-else-return': 'error',
        'no-implied-eval': 'error',
        'no-implicit-globals': 'error',
        'no-implicit-coercion': 'error',
        'no-floating-decimal': 'error',
        'no-extra-label': 'error',
        'no-nested-ternary': 'error',
        'no-param-reassign': 'error',
        'no-unneeded-ternary': 'error',
        'no-unused-expressions': 'error',
        'no-useless-call': 'error',
        'no-useless-catch': 'error',
        'prefer-const': 'error',
        'yoda': 'error',
        'array-bracket-newline': [
            'error',
            'consistent',
        ],
        'array-element-newline': [
            'error',
            'consistent',
        ],
        'brace-style': 'error',
        'comma-dangle': [
            'error',
            'always-multiline',
        ],
        'comma-spacing': [
            'error',
            {
                before: false,
                after: true
            }
        ],
        'implicit-arrow-linebreak': 'error',
        'indent': [
            'error',
            4,
            {SwitchCase: 1}
        ],
        'no-tabs': 'error',
        'object-curly-newline': [
            'error',
        ],
        'semi': 'error',
    }
};
